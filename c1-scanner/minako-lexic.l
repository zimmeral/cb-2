%option noyywrap yylineno nounput noinput

%{
#include "minako.h"
%}

DIGIT [0-9]
INTEGER ({DIGIT}+)
FLOAT ({INTEGER}"."{INTEGER}|"."{INTEGER})
LETTER [a-zA-Z]
ID ({LETTER})+({DIGIT}|{LETTER})*

%x COMMENT

%%

bool return KW_BOOLEAN;
do return KW_DO;
else return KW_ELSE;
float return KW_FLOAT;
for return KW_FOR;
if return KW_IF;
int return KW_INT;
printf return KW_PRINTF;
return return KW_RETURN;
void return KW_VOID;
while return KW_WHILE;

"==" return EQ;
"!=" return NEQ;
"<" return LSS;
">" return GRT;
"<=" return LEQ;
">=" return GEQ;
"&&" return AND;
"||" return OR;

{INTEGER} {
  yylval.intValue=atoi(yytext);
  return CONST_INT;
}
({FLOAT}([eE][\-+]?{INTEGER})?)|({INTEGER}[eE][\-+]?{INTEGER}) {
  yylval.floatValue=atof(yytext);
  return CONST_FLOAT;
}
"true"|"false"  {
  yylval.intValue=strcmp(yytext,"true")==0 ? 1 : 0;
  return CONST_BOOLEAN;
}
\"[^\n\"]*\" {
  yytext[yyleng-1]='\0';
  yytext++;
  yylval.string = yytext;
  return CONST_STRING;
}
{ID}            { yylval.string = yytext; return ID; }

[+\-\*/=,;\(})\{\}] return yytext[0];

"/*"            { BEGIN(COMMENT); }
<COMMENT>"*/"   { BEGIN(INITIAL); }
<COMMENT>.|\n

"//"[^\n]*[\n]  { }

[ \t\n]+         { }

<<EOF>>         { return EOF; }

%%
