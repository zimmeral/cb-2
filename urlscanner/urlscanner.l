%option noyywrap yylineno nounput noinput

%{
#include "urlscanner.h"
%}

%s SEARCH_HREF
%s SEARCH_TEXT

%%

"<a" {
  BEGIN(SEARCH_HREF);
}

<SEARCH_HREF>"href=\""[^\"]*"\"" {
  yytext[yyleng-1]='\0';
  yytext+=6;
  yylval=yytext;
  BEGIN(SEARCH_TEXT);
  return TOKEN_URL;
}

<SEARCH_TEXT>">"[^<]* {
  yytext+=1;
  yylval=yytext;
  BEGIN(INITIAL);
  return TOKEN_TEXT;
}

(.|\n) {}

<<EOF>> return EOF;

%%
